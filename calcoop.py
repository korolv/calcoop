#!/usr/bin/env python3
import sys
import subprocess

startinp = 0
class Calc:

	def full():
		Calc.start()
		Calc.endf()

	def __init__(self):
		self.data = []
	
	def counts():
		global x
		global y
		x = str(input("Print first:"))
		y = str(input("Print second:"))
	
	def add():
		Calc.counts()
		global z
		z = (str(float(x) + float(y))+"\n")
		Calc.infile()
		print(z)

	def devide():
		Calc.counts()
		global z
		z = (str(float(x) / float(y))+"\n")
		Calc.infile()	
		print(z)

	def start():
		print("\nEnter 'options' to menu\n")
	
	def options():
		print("\nEnter 'add' to add two numbers\n")
		print("Enter 'devide' to devide two numbers\n")
		print("Enter 'multiply' to multiply two numbers\n")
		print("Enter 'subs' to substract two numbers\n")
		print("Enter 'options' to menu\n")
		print("Enter 'quit' to quit\n")
		print("Enter 'log' to chech log-file\n")

	def checklog():
		subprocess.call('less logcalcoop.log' , shell=True)

	def multiply():
		Calc.counts()
		global z
		z = (str(float(x)*float(y))+"\n")
		Calc.infile()
		print(z)

	def substract():
		global z
		Calc.counts()
		z = (str(float(x)-float(y))+"\n")
		Calc.infile()
		print(z)

	def cicle():
		try:
			while True:
				global startinp
				startinp = input(":")
				if startinp == "options":
					Calc.options()
				elif startinp == "devide":
					Calc.devide()
					break
				elif startinp == "multiply":
					Calc.multiply()
					break
				elif startinp == "add":
					Calc.add()
					break
				elif startinp == "subs":
					Calc.substract()
					break
				elif startinp == "quit" or startinp == "q":
					exit(0)
				elif startinp == "log":
					Calc.checklog()
				else:
					print("Unexpected unput!")
					continue
		except ZeroDivisionError:
			print("Gay detected!")
		except ValueError:
			print("try again")
		except KeyboardInterrupt:
			print("BYE")
			exit(0)			
		except EOFError:
			print("BYE")	
			exit(0)
	
	def endf():
		while True:
			Calc.cicle()
			end = input("one more operation?(y/n)\n:")
			if end == "y":
				Calc.options()
				continue
			if end == "quit":
				exit(0)
			else:
				print("BYE")
				exit(0)
	
	def infile():
		if startinp == 'add' or sys.argv[1] == "--add" or sys.argv[1] == "-a":
			f = open("logcalcoop.log","a")
			f.write(x + " + " + y + " = " + z)
			f.close()
		elif startinp == 'devide' or sys.argv[1] == '-d' or sys.argv == '--devide':
			f = open("logcalcoop.log","a")
			f.write(x + " / " + y + " = " + z)
			f.close()
		elif startinp == 'multiply' or sys.argv[1] == '-m' or sys.argv == '--multiply':
			f = open("logcalcoop.log","a")
			f.write(x + " x " + y + " = " + z)
			f.close()
		elif startinp == 'substract' or sys.argv[1] == '-s' or sys.argv == '--substract':
			f = open("logcalcoop.log","a")
			f.write(x + " - " + y + " = " + z)
			f.close()

if __name__ == "__main__":
	if len(sys.argv) == 1:
		print("Usage:")
		print("-o to options")
		print("-a or --add to add two numbers")
		print("-d to devide two numbers")
		print("-s to substract two numbers")
		print("-m to multiply two numbers")
		print("-l to check log-file")
		print("-e to enter function")
	elif len(sys.argv) > 1:
		if sys.argv[1] == "-a" or sys.argv[1] == "--add" :
			Calc.add()
			Calc.full()
		elif sys.argv[1] == "-o":
			Calc.options()
			Calc.endf()
		elif sys.argv[1] == "-d":
			Calc.devide()
			Calc.endf()
		elif sys.argv[1] == "-m":
			Calc.multiply()
			Calc.endf()
		elif sys.argv[1] == "-s":
			Calc.substract()
			Calc.endf()
		elif sys.argv[1] == "-l":
			Calc.checklog()
		elif sys.argv[1] == "-e":
			Calc.endf()
		else:
			print("Dolbaeb, vvedi norm opcii")
	else:
			print("Dolbaeb, vvedi norm opcii")
